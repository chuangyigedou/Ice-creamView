﻿/******************************************
 * 姓名 : #AuthorName#
 * 时间 : #CreateTime#
 * 电话 : #Phone#
 * 邮箱 : #Email#
 * 简要 :
 ******************************************
 */
using System.Collections.Generic;
using UnityEngine;
using System;

public class List_Sort : MonoBehaviour
{
    public List<Data> List_Datas = new List<Data>();
    private void Start()
    {
        for (int i = 0; i < 10000; i++)
        {
            Data t = new Data { id = UnityEngine.Random.Range(0, 100000000), name = "str_" + i.ToString() };
            List_Datas.Add(t);
        }


    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            List_Datas.Sort((left, right) => { var result = left.id - right.id; return result; });
        }
    }
}

[Serializable]
public class Data
{
    public int id;
    public string name;
}