﻿
using System.Collections.Generic;
using UnityEngine;

public class List_ForEach : MonoBehaviour
{
    public List<Data> List_Datas = new List<Data>();
    void Start()
    {

        for (int i = 0; i < 5; i++)
        {
            Data t = new Data { id = i, name = "str_" + i.ToString() };
            List_Datas.Add(t);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            List_Datas.ForEach(item =>
           {
               Debug.Log(item.name);
               if (item.id == 1)
               {
                   List_Datas.Remove(item);
                   List_Datas.Add(new Data { id = 10, name = "罗" });
                   List_Datas.Add(new Data { id = 10, name = "家" });
                   List_Datas.Add(new Data { id = 10, name = "胜" });
                   return;
               }
           });

        }
    }
}
