﻿using UnityEngine;
using UnityEngine.UI;
using IcecreamView;

public class viewmodule_toview : IC_AbstractModule
{
    public Button toview;
    public bool isClose;

    public string viewTable;

    public override void OnInitView()
    {
        if (toview != null) {
            toview.onClick.AddListener(()=> {
                viewConnector.OpenView(viewTable , isClose);
            });
        }
    }
}
