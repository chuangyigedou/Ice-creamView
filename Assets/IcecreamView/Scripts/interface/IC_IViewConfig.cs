﻿using System.Collections.Generic;

namespace IcecreamView
{
    public interface IC_IViewConfig
    {
        void OnInit();

        string GetDefaultViewTable();

        bool ContainsKey(string viewTable);

        IC_IViewInfo GetViewModle(string viewTable);

        void OnDispose();
    }
}

