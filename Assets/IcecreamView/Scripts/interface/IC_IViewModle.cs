﻿
namespace IcecreamView {
    public interface IC_IViewInfo
    {
        string GetTable();
        bool IsOnce();
        IC_AbstractView GetView();
    }
}

