﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IcecreamView
{
    /// <summary>
    /// GameView模板类
    /// </summary>
    [RequireComponent(typeof(IC_ModuleConnector))]
    public abstract class IC_AbstractModule : MonoBehaviour
    {
        [Header("执行优先级")]
        public int prioritylevel = 1;

        [HideInInspector]
        public IC_ModuleConnector viewConnector;

        public virtual void OnOpenView() { }

        public virtual void OnCloseView() { }

        public virtual void OnInitView() { }

        public virtual void OnDestoryView() { }

    }
}

